const mongoose = require('mongoose');

const productSchema = new mongoose.Schema(
	{
		productName: {
			type: String,
			required: [true, "Product Name is required"]
		},
		productDescription: {
			type: String,
			required: [true, "Product Description is required"]
		},
		price: {
			type: Number,
			required: [true, "Price is required"]
		},
		isActive: {
			type: Boolean,
			default: true
		}, 
		createdOn: {
			type: Date,
			default: new Date()
		}, 
		orders: [
		{
			customerId: {
				type: String,
				required: [true, "Customer ID is required"]
			},
			totalAmount: {
				type: Number,
				required: [true, "Customer ID is required"]
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}

		]

	}

);

module.exports = mongoose.model("Product", productSchema);
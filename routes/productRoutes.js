const express = require('express');
const router = express.Router();

//Controller
const productController = require("./../controllers/productControllers");

//Authentication
const auth = require('./../auth');

//Create New Product - Admin Only
router.post('/products', auth.verify, (req, res) => {

	let userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin === true){
		productController.createProduct(req.body).then(result => res.send(result));
	} else {
		res.send("Not Authorized")
		
	}
})


// Retrieve All Active Product
router.get('/products', (req, res) => {
	productController.getAllProduct(req.body).then(result => res.send(result));
})


//Retrieve Single Product
router.get('/products/:productId', (req, res) => {
	productController.getSingleProduct(req.params).then( result=> res.send (result))
})


//Update Product Info - Admin Only
router.put('/products/:productId', auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin === true){
		productController.updateProductInfo(req.params.productId, req.body).then( result => res.send (result))
	} else {
		res.send("Not Authorized")
		
	}
})


//Archive Product - Admin Only
router.put('/products/:productId/archive', auth.verify, (req, res) => {

	let userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin === true){
		productController.archiveProduct(req.params.productId).then( result => res.send(result))
	} else {
		res.send("Not Authorized")
		
	}
	
})


//Unarchive Product - Admin Only
router.put('/products/:productId/unarchive', auth.verify, (req, res) => {

	let userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin === true){
		productController.unArchiveProduct(req.params.productId).then( result => res.send(result))
	} else {
		res.send("Not Authorized")
		
	}
	
})


//Delete Product - Admin Only
router.delete('/products/:productId/delete', auth.verify, (req, res) => {

	let userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin === true){
		productController.deleteProduct(req.params.productId).then( result => res.send(result))
	} else {
		res.send("Not Authorized")
		
	}
	
})



module.exports = router;

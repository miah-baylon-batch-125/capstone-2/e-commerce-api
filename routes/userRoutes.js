const express = require('express');
const router = express.Router();

//Controller
const userController = require("./../controllers/userControllers");

//Authentication
const auth = require('./../auth');


//User Registration
router.post("/register", (req, res) => {
	userController.register(req.body).then(result => res.send(result))
});


//Login
router.post("/login", (req, res) => {

	userController.login(req.body).then( result => res.send(result))
})


//Checking if email already exists
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(result => res.send(result))
})


//Set User as Admin
router.put('/:userId/setAsAdmin', auth.verify, (req, res, error) => {

	let userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin === true){
		userController.setAsAdmin(req.params.userId).then(result => res.send(result))
	} else {
		res.send("Not Authorized")
	}
})


//Create an Order (Non-Admin)
router.post('/checkout', auth.verify, (req,res) => {
	

	let data = {	
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		quantity: req.body.quantity
		
	}

	let userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin === false){
		userController.createOrder(data).then(result => res.send(result))
	} else {
		res.send("Only users are allowed to place an order!")
	} 

})

//Retrieve all Orders (Admin Only)
router.get('/orders', auth.verify, (req, res) => {


	let userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin === true){
		userController.retrieveAllOrder(req.body).then(result => res.send(result))
	} else {
		res.send("Only admins are allowed to retrieve orders!")
	} 

})


//Retrieve all Orders per Product (Admin Only)
router.get('/orders/:productId', auth.verify, (req, res) => {

	let userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin === true){
		userController.retrieveProductOrder(req.params).then(result => res.send(result))
	} else {
		res.send("Only admin are allowed to retrieve orders!")
	} 

})


//Retrieve Orders (Non-Admin Only)
router.get("/myOrders", auth.verify, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id
	}

	let userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin === false){
		userController.retrieveMyOrder(data).then(result => res.send(result))
	} else {
		res.send("You are not authorized to retrieve orders!")
	} 

});


module.exports = router;

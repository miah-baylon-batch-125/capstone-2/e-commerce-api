const Product = require("./../models/Product")

//Create New Product - Admin Only
module.exports.createProduct = (reqBody) => {
let newProduct = new Product({
		productName: reqBody.productName,
		productDescription: reqBody.productDescription,
		price: reqBody.price

	})
	return newProduct.save().then( (product, error) =>{
		if(error){
			return false
		} else {
			return product 
		}
	})
} 


// Retrieve All Active Product
module.exports.getAllProduct = () => {
	return Product.find({isActive: true}).then( result => {
		return result
	})
}


//Retrieve Single Product
module.exports.getSingleProduct = (params) => {
	return Product.findById(params.productId).then (product => {
		return product
	})
}


//Update Product Info - Admin Only
module.exports.updateProductInfo = (params, reqBody) => {

	let updatedProduct = {
		productName: reqBody.productName,
    	productDescription: reqBody.productDescription,
    	price: reqBody.price
	}

	console.log(reqBody)

	return Product.findByIdAndUpdate(params, updatedProduct, {new: true})
	.then((result, error) => {
		if(error){
			return error
		} else {
			return result
		}
	})
}


//Archive Product - Admin Only
module.exports.archiveProduct = (params) => {

	let archiveProduct = {
		isActive: false
	}

	return Product.findByIdAndUpdate(params, archiveProduct, {new: true})
	.then( (result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}


//Unarchive Product - Admin Only
module.exports.unArchiveProduct = (params) => {

	let archiveProduct = {
		isActive: true
	}

	return Product.findByIdAndUpdate(params, archiveProduct, {new: true})
	.then( (result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}


//Delete Product - Admin Only
module.exports.deleteProduct = (params) => {

	return Product.findByIdAndDelete(params)
	.then( (result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}


const User = require("./../models/User")
const Product = require("./../models/Product")
const bcrypt = require('bcrypt');
const auth = require('./../auth');

//User Registration
module.exports.register = (reqBody) => {

	return User.findOne({email: reqBody.email}).then( (result) => {

		if (result == null) {
			let newUser = new User({
				firstName: reqBody.firstName,
				lastName: reqBody.lastName,
				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password, 10)
			})
				return newUser.save().then((result, error) => {
					if(error){
						return error
					} else {
						return result
					}
				})
		} else {
			return ('Email Already Exists. Try using a different email!')
		}
	})
}

//Login
module.exports.login = (reqBody) => { 
	return User.findOne({email: reqBody.email}).then( (result) => {

		if(result == null){
			return ('E-mail not yet registered!')

		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect === true){
				return { access: auth.createAccessToken(result.toObject())}
			} else {
				return ('Password incorrect. Try Again!')
			}
		}
	})
}


//Checking if email already exists
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email})
	.then( (result) => {
		if(result.length != 0){
			return true
		} else {
			return false
		}
	})
}



//Set User As Admin
module.exports.setAsAdmin = (params) => {

	let newAdmin = {
		isAdmin: true
	}

	return User.findByIdAndUpdate(params, newAdmin, {new: true})
	.then( (result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}



//Create Order (Non-Admin)
module.exports.createOrder = async(data) => {

	var productPrice = await Product.findById(data.productId).then(product => product.price)

	//Save User Order
		const userSaveOrder = await User.findById(data.userId).then( user => {
			
			
			let totalAmountOrder = (data.quantity) * (productPrice)
			user.order.push(
					{	products: {
							productId: data.productId,
							quantity: data.quantity
							},
						totalAmount: totalAmountOrder

					}
			)


			return user.save().then( (user, error) => {
					if (error){
						return false
					} else {
						return true
					}
				})

			})

	//Save Product Orders
		const productSaveOrder = await Product.findById(data.productId).then(product => {

			let totalAmountOrder = (data.quantity) * (productPrice)

			product.orders.push(
				{
					customerId: data.userId,
					totalAmount: totalAmountOrder
				})

			return product.save().then( (order, error) => {
					if (error){
						return false
					} else {
						return true
					}
			})
		})
		
		if(userSaveOrder && productSaveOrder){
			return ('Order saved!')
		} else {
			return false
		}

}

//Retrieve all Orders (Admin Only)
module.exports.retrieveAllOrder = () => {
    return Product.find().then(productOrder => {
        let allOrders = [];
        productOrder.forEach(productOrder => {
            allOrders.push({
                product: productOrder.productName,
                orders: productOrder.orders
            });
        })
        return allOrders;
    })
}



//Retrieve all Orders per Product (Admin Only)
module.exports.retrieveProductOrder = (params) => {
	return Product.findById(params.productId).then( result => {
		return result.orders
	})
}


//Retrieve Orders (Non-Admin Only)
module.exports.retrieveMyOrder = (data) => {
    return User.findById(data.userId).then(user => {
        return user.order;
    })
};




const express = require('express');
const mongoose = require('mongoose');
const cors = require("cors");
const PORT = process.env.PORT || 3000;
const app = express();

let userRoutes = require("./routes/userRoutes");
let productRoutes = require("./routes/productRoutes")

app.use(express.json());
app.use(express.urlencoded({extended: true}))
app.use(cors());

mongoose.connect("mongodb+srv://ppr0119:admin@cluster0.pzreh.mongodb.net/e-commerce?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true, 
		useUnifiedTopology: true
	}
)
.then( () => console.log(`Connected to Database`))
.catch( (error) => console.log(error))



app.use("/ecommerce/users", userRoutes);
app.use("/ecommerce/products", productRoutes)

app.listen(PORT, () => console.log(`Server running at port ${PORT}`));